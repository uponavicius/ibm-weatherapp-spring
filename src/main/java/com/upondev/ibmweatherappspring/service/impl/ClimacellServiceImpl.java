package com.upondev.ibmweatherappspring.service.impl;

import com.upondev.ibmweatherappspring.model.entity.ClimacellDataEntity;
import com.upondev.ibmweatherappspring.model.repository.ClimacellDataRepository;
import com.upondev.ibmweatherappspring.model.response.ResponseFromClimacellModel;
import com.upondev.ibmweatherappspring.service.ClimacellService;
import com.upondev.ibmweatherappspring.util.DateUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class ClimacellServiceImpl implements ClimacellService {
    final
    ClimacellDataRepository climacellDataRepository;

    public ClimacellServiceImpl(ClimacellDataRepository climacellDataRepository) {
        this.climacellDataRepository = climacellDataRepository;
    }

    @Scheduled(fixedDelay = 60 * 60 * 1000)
    @Override
    public void updateDataBase() {
        System.out.println("Updating DB...");
        if (climacellDataRepository.countAllBy() == 0) {
            LocalDateTime dateFrom = LocalDateTime.now().minusMonths(1);
            LocalDateTime dateTo = LocalDateTime.now().plusDays(1);

            List<LocalDateTime> dates = DateUtils.getDatesBetweenUsingJava8(dateFrom, dateTo);
            for (int i = 0; i < dates.size() - 1; i++) {
                String dateF = dates.get(i).toString().substring(0, 19) + "Z";
                String dateT = dates.get(i + 1).toString().substring(0, 19) + "Z";
                dataFromResponseToDB(dateF, dateT);
            }
        } else {
            LocalDateTime dateFrom = climacellDataRepository
                    .findTopByOrderByObservationTimeValueDesc()
                    .getObservationTimeValue();
            LocalDateTime dateTo = LocalDateTime.now().plusDays(2);

            List<LocalDateTime> dates = DateUtils.getDatesBetweenUsingJava8(dateFrom, dateTo);
            for (int i = 0; i < dates.size() - 1; i++) {
                String dateF = dates.get(i).toString() + ":00Z";
                String dateT = dates.get(i + 1).toString() + ":00Z";
                dataFromResponseToDB(dateF, dateT);
            }
        }

    }

    @Override
    public List<ClimacellDataEntity> getVilniusWeatherFromDB(String dateString) {
        LocalDateTime from = LocalDateTime.parse(dateString + "T00:00");
        LocalDateTime to = LocalDateTime.parse(dateString + "T23:59");
        List<ClimacellDataEntity> data = climacellDataRepository.findByObservationTimeValueBetween(from, to);
        return data;
    }

    @Override
    public String getMinDate() {
        LocalDateTime date = climacellDataRepository.findTopByOrderByObservationTimeValueAsc().getObservationTimeValue();
        return date.toString().substring(0, 10);
    }

    private void dataFromResponseToDB(String dateFrom, String dateTo) throws HttpClientErrorException {
        String getUrl = "https://api.climacell.co/v3/weather/historical/station?lat=54.687157&lon=25.279652&unit_system=si" +
                "&start_time=" + dateFrom +
                "&end_time=" + dateTo +
                "&fields=temp&apikey=CBNp2fghd9rgNDPgMQMFuJqDNsBzD0u6";

        RestTemplate restTemplate = new RestTemplate();
        ResponseFromClimacellModel[] response = restTemplate.getForObject(getUrl, ResponseFromClimacellModel[].class);

        if (response != null) {
            List<ResponseFromClimacellModel> data = Arrays.asList(response);

            // if the database has an entry
            if (climacellDataRepository.countAllBy() > 0) {
                String lastRecordDateTime = climacellDataRepository.findTopByOrderByObservationTimeValueDesc().getObservationTimeValue().toString();

                for (ResponseFromClimacellModel datum : data) {
                    String dateFromResponse = LocalDateTime.parse(datum.getObservationTime().get("value").toString().substring(0, 16)).toString();

                    // If date from DB and date from response object are equals
                    if (!lastRecordDateTime.equals(dateFromResponse)) {
                        recordingToDB(datum);
                    }
                }
            } else { // if the database hasn't an entry
                for (ResponseFromClimacellModel datum : data) {
                    recordingToDB(datum);
                }

            }
        }
    }

    private void recordingToDB(ResponseFromClimacellModel responseFromClimacell) {
        ClimacellDataEntity climacell = new ClimacellDataEntity();
        climacell.setTempValue(
                Double.parseDouble(responseFromClimacell.getTemp().get("value").toString()));
        climacell.setTempUnits(
                responseFromClimacell.getTemp().get("units").toString().charAt(0));
        climacell.setObservationTimeValue(
                LocalDateTime.parse(responseFromClimacell.getObservationTime().get("value").toString().substring(0, 19)));
        climacellDataRepository.save(climacell);
    }

}
