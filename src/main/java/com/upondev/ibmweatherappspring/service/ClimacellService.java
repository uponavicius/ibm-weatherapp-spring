package com.upondev.ibmweatherappspring.service;

import com.upondev.ibmweatherappspring.model.entity.ClimacellDataEntity;

import java.util.List;

public interface ClimacellService {

    void updateDataBase();

    List<ClimacellDataEntity> getVilniusWeatherFromDB(String dateString);

    String getMinDate();
}
