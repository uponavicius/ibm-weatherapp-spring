package com.upondev.ibmweatherappspring.controller;

import com.upondev.ibmweatherappspring.model.entity.ClimacellDataEntity;
import com.upondev.ibmweatherappspring.service.impl.ClimacellServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("")
@CrossOrigin(origins = "*")
public class WeatherController {

    final
    ClimacellServiceImpl climacellService;

    public WeatherController(ClimacellServiceImpl climacellService) {
        this.climacellService = climacellService;
    }

    @ApiOperation(value = "Get Vilnius weather history from DB.", notes = "${weatherController.GetVilniusWeatherFromDB.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "date", value = "Date format yyyy-MM-dd")
    })
    @GetMapping(path = "/weather")
    public List<ClimacellDataEntity> getVilniusWeatherFromDB(@RequestParam String date) {
        return climacellService.getVilniusWeatherFromDB(date);
    }

    @ApiOperation(value = "Get oldest record date.", notes = "${weatherController.GetMinDate.ApiOperation.Notes}")
    @GetMapping(path = "minDate")
    public List<String> getMinDate() {
        return Arrays.asList(climacellService.getMinDate());
    }
}
