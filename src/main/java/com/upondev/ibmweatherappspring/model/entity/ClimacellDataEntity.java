package com.upondev.ibmweatherappspring.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "climacell_data")
public class ClimacellDataEntity implements Serializable {
    private static final long serialVersionUID = -8969688817096202068L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private double tempValue;

    @Column
    private char tempUnits;

    @Column
    private LocalDateTime observationTimeValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTempValue() {
        return tempValue;
    }

    public void setTempValue(double tempValue) {
        this.tempValue = tempValue;
    }

    public char getTempUnits() {
        return tempUnits;
    }

    public void setTempUnits(char tempUnits) {
        this.tempUnits = tempUnits;
    }

    public LocalDateTime getObservationTimeValue() {
        return observationTimeValue;
    }

    public void setObservationTimeValue(LocalDateTime observationTimeValue) {
        this.observationTimeValue = observationTimeValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClimacellDataEntity that = (ClimacellDataEntity) o;
        return id == that.id &&
                Double.compare(that.tempValue, tempValue) == 0 &&
                tempUnits == that.tempUnits &&
                Objects.equals(observationTimeValue, that.observationTimeValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tempValue, tempUnits, observationTimeValue);
    }
}
