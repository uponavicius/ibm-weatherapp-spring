package com.upondev.ibmweatherappspring.model.repository;

import com.upondev.ibmweatherappspring.model.entity.ClimacellDataEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ClimacellDataRepository extends CrudRepository<ClimacellDataEntity, Long> {

    long countAllBy();

    ClimacellDataEntity findTopByOrderByObservationTimeValueDesc();

    ClimacellDataEntity findTopByOrderByObservationTimeValueAsc();

    List<ClimacellDataEntity> findByObservationTimeValueBetween(LocalDateTime from, LocalDateTime to);
}
