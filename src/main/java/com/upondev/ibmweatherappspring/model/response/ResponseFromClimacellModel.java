package com.upondev.ibmweatherappspring.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class ResponseFromClimacellModel {

    private Map<String, Object> temp;

    @JsonProperty("observation_time")
    private Map<String, Object> observationTime;


    public Map<String, Object> getTemp() {
        return temp;
    }

    public void setTemp(Map<String, Object> temp) {
        this.temp = temp;
    }

    public Map<String, Object> getObservationTime() {
        return observationTime;
    }

    public void setObservationTime(Map<String, Object> observationTime) {
        this.observationTime = observationTime;
    }

    @Override
    public String toString() {
        return "ClimacellResponseModel{" +
                ", temp=" + temp +
                ", observationTime=" + observationTime +
                '}';
    }
}
