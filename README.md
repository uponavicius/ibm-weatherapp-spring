# ibm-weatherapp-spring

Weatherapp REST API 

Back-End REST API was created using Spring Boot framework. Back-End working with MySQL database.


#### To start project:
* Clone repository https://gitlab.com/uponavicius/ibm-weatherapp-spring.git
* Open with your favorite IDEA (example IntelliJ IDEA)
* Create schema in MySQL:
* Start MySQL database locally

Run commands:
* create schema YOUR_DATABASE_NAME;
* grant all privileges on YOUR_DATABASE_NAME to 'YOUR_USER'@'%';


* Back to your favorite IDEA and go to folder `src/main/resources` then open file `application.properties`.
Change user name, pasword and database name;

`spring.datasource.username=MYSQL_USER`

`spring.datasource.password=MYSQL_PASSWORD`

`spring.datasource.url=jdbc:mysql://localhost:3306/YOUR_DATABASE_NAME`


* Run project
* Swagger documentation http://localhost:8080/swagger-ui.html

* Get history of Vilnius weather (one day)- http://ec2-3-122-60-105.eu-central-1.compute.amazonaws.com:8080/ibm/minDate
* Get the oldest record date - http://ec2-3-122-60-105.eu-central-1.compute.amazonaws.com:8080/ibm/weather?date=2020-07-03


#### Front-End project 
* Run this Angular project `https://gitlab.com/uponavicius/ibm-weatherapp-angular`

#### Rest Assured Tests project
* Run this Angular project `https://gitlab.com/uponavicius/ibm-weatherapp-spring-rest-assured-tests`



